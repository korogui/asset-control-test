package ee.korogui.monitor.service;

import ee.korogui.monitor.model.SeveritySummary;
import org.springframework.stereotype.Service;

@Service
public class MonitorService {

    private SeveritySummary latestSeveritySummary = SeveritySummary.builder().build();

    public SeveritySummary lastestServeritySummary() {
        return latestSeveritySummary;
    }

    public void setLatestSeveritySummary(SeveritySummary latestSeveritySummary) {
        this.latestSeveritySummary = latestSeveritySummary;
    }
}
