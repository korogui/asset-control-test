package ee.korogui.monitor.web;

import ee.korogui.Application;
import ee.korogui.monitor.model.SeveritySummary;
import ee.korogui.monitor.model.RequestLogInfoChange;
import ee.korogui.monitor.service.MonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MonitorController {

    @Autowired
    private MonitorService monitorService;

    @MessageMapping("/changeInterval")
    public void requestChange(RequestLogInfoChange requestLogInfoChange) {
        if (requestLogInfoChange != null && requestLogInfoChange.getMonitoringInterval() > 0) {
            final String interval = String.valueOf(requestLogInfoChange.getMonitoringInterval());
            System.setProperty(Application.ENV_MONITOR_INTERVAL, interval);
        }
    }

    @MessageMapping("/severitySummary")
    public SeveritySummary getLastestSeveritySummary() {
        return monitorService.lastestServeritySummary();
    }

}
