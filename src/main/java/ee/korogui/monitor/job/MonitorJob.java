package ee.korogui.monitor.job;

import ee.korogui.monitor.model.SeveritySummary;
import ee.korogui.monitor.service.MonitorService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.input.ReversedLinesFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ee.korogui.Application.DEFAULT_MONITOR_INTERVAL;
import static ee.korogui.Application.ENV_MONITOR_INTERVAL;

@Service
@Slf4j
public class MonitorJob {

    public static final int TASK_INTERVAL_MILLISECONDS = 5000;

    @Value("${monitor.filePath}")
    private String ENV_MONITOR_FILE_PATH;

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private MonitorService service;

    @Autowired
    private Environment environment;

    // Message line pattern would be <date_time> <severity> <message>
    // Severity types: INFO, WARNING or ERROR
    // e.g.: 2016-09- 20 16:23:14,994 ERROR Some error message
    private static final String MESSAGE_PATTERN = "^(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2},\\d{3}) ((INFO)|(WARNING)|(ERROR)) \\w*";

    private final Pattern pattern = Pattern.compile(MESSAGE_PATTERN);

    final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");

    @Scheduled(fixedDelay = TASK_INTERVAL_MILLISECONDS)
    private void sendMessage() {
        process();
    }

    private void process() {
        final long start = System.nanoTime();
        log.debug("Monitoring read job started!");
        try {
            final int monitoringInterval = getMonitoringInterval();

            final SeveritySummary previousSummary = service.lastestServeritySummary();
            final SeveritySummary summary = getSeveritySummary(monitoringInterval);

            service.setLatestSeveritySummary(summary);

            if (summary != null && (previousSummary == null || !previousSummary.equalsSeverity(summary))) {
                this.template.convertAndSend("/topic/severitySummary", summary);
            }
        } catch (IOException exception) {
            exception.printStackTrace();
            log.error("Error reading monitoring file!", exception);
        }
        final long end = TimeUnit.MILLISECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS);
        log.info(String.format("Monitoring read job finished! %dms", end));
    }

    private int getMonitoringInterval() {
        final Integer interval = environment.getProperty(ENV_MONITOR_INTERVAL, Integer.class);
        return interval != null ? interval : DEFAULT_MONITOR_INTERVAL;
    }

    private SeveritySummary getSeveritySummary(final int seconds) throws IOException {
        final LocalDateTime now = LocalDateTime.now();
        final LocalDateTime minDateTime = now.minusSeconds(seconds);

        final SeveritySummary summary = SeveritySummary.builder().dateTime(now).build();

        final File file = new File(ENV_MONITOR_FILE_PATH);
        if (!file.exists()) {
            throw new FileNotFoundException("Monitoring file is missing");
        }

        final ReversedLinesFileReader fileReader = new ReversedLinesFileReader(file, Charset.defaultCharset());
        try {
            String line = fileReader.readLine();
            while (line != null) {
                final Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    final String dateTimeString = matcher.group(1);
                    final LocalDateTime localDateTime = LocalDateTime.parse(dateTimeString, dateTimeFormatter);

                    if (minDateTime.isAfter(localDateTime)) {
                        break;
                    }

                    final String severity = matcher.group(2);
                    if ("INFO".equals(severity)) {
                        summary.incrementInfo();
                    } else if ("WARNING".equals(severity)) {
                        summary.incrementWarning();
                    } else if ("ERROR".equals(severity)) {
                        summary.incrementError();
                    }
                }

                line = fileReader.readLine();
            }
        } finally {
            fileReader.close();
        }

        return summary;
    }

}
