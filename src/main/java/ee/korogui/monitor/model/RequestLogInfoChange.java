package ee.korogui.monitor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class RequestLogInfoChange {

    private int monitoringInterval;
}
