package ee.korogui.monitor.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Builder
@Setter
@Getter
public class SeveritySummary {

    private LocalDateTime dateTime;

    private long info;

    private long warning;

    private long error;

    public synchronized void incrementInfo() {
        info++;
    }

    public synchronized void incrementWarning() {
        warning++;
    }

    public synchronized void incrementError() {
        error++;
    }

    public boolean equalsSeverity(SeveritySummary summary) {

        return summary != null
                && this.info == summary.info
                && this.warning == summary.warning
                && this.error == summary.error;
    }
}
