package ee.korogui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableAutoConfiguration
@EnableScheduling
@ComponentScan
public class Application {

    public static final String ENV_MONITOR_INTERVAL = "monitor.intervalSeconds";
    public static final int DEFAULT_MONITOR_INTERVAL = 5;

    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
