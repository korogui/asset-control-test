import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

require('./assets/css/main.css');
require('./assets/css/bulma.css');

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
